<?php
/*
Plugin Name: Am2b - Favicon
Plugin URI: am2bmarketing.co.th
Description: Am2b Software development team.
WordPress admin interface
Version: 1.0
Author: Am2b Dev Team
Author URI: am2bmarketing.co.th
License: GPLv2
*/

add_action( 'wp_head', 'am2b_change_favicon');
function am2b_change_favicon () {
    $site_icon_url = get_site_icon_url();

    if ( !empty( $site_icon_url ) ) {
        wp_site_icon();
    } else {
        $icon_url = plugins_url( 'favicon.ico', __FILE__ );
        ?>
        <link rel="shortcut icon" href="<?php echo $icon_url; ?>" />
    <?php }
}
?>
