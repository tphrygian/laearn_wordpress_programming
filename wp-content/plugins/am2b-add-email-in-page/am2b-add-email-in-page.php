<?php
/*
Plugin Name: Am2b - Add Email in Page
Plugin URI: am2bmarketing.co.th
Description: Am2b Software development team.
WordPress admin interface
Version: 1.0
Author: Am2b Dev Team
Author URI: am2bmarketing.co.th
License: GPLv2
*/

add_filter( 'the_content', 'am2b_add_email_page');
function am2b_add_email_page ( $the_content ) {
    $mail_icon_url = plugins_url( 'mailicon.png', __FILE__ );
    $new_content = $the_content;

    $new_content .= '<div class="email_link">';
    $new_content .= '<a href="mailto:someone@somewhere.com?';
    $new_content .= 'subject=Check out this interesting article ';
    $new_content .= 'entitled ' . get_the_title();
    $new_content .= '&body=Hi!%0A%0AI thought you would enjoy ';
    $new_content .= 'this article entitled ';
    $new_content .= get_the_title() . '.%0A%0A' . get_permalink();
    $new_content .= '%0A%0AEnjoy!">';

    if ( !empty( $mail_icon_url ) ) {
        $new_content .= '<img alt="Email icon" ';
        $new_content .= ' src="';
        $new_content .= $mail_icon_url. '" /></a>';
    } else {
        $new_content .= 'Email link to this article';
    }
    $new_content .= '</div>';

    // Return filtered content for display on the site
    return $new_content;
}