<?php
/*
Plugin Name: Am2b - Add Short Code
Plugin URI: am2bmarketing.co.th
Description: Am2b Software development team.
WordPress admin interface
Version: 1.0
Author: Am2b Dev Team
Author URI: am2bmarketing.co.th
License: GPLv2
*/

add_shortcode( 'private', 'am2b_add_private_shortcode' );
function am2b_add_private_shortcode ( $atts, $content = null ) {
    if ( is_user_logged_in() ) {
        return '<div class="private">' . $content . '</div>';
    } else {
        $output = '<div class="register">';
        $output .= 'You need to become a member to access ';
        $output .= 'this content.</div>';
        return $output;
    }
}