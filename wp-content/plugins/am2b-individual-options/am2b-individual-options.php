<?php
/*
Plugin Name: Am2b - Individual Options
Plugin URI: am2bmarketing.co.th
Description: Am2b Software development team.
WordPress admin interface
Version: 1.0
Author: Am2b Dev Team
Author URI: am2bmarketing.co.th
License: GPLv2
*/

register_activation_hook( __FILE__, 'am2b_set_default_options' );
function am2b_set_default_options () {
    if ( false === get_option( 'am2b_ga_account_name' ) ) {
        add_option( 'am2b_ga_account_name', 'UA-0000000-0' );
    }
}

?>