<?php
/*
Plugin Name: Am2b - Change Meta Tag
Plugin URI: am2bmarketing.co.th
Description: Am2b Software development team.
WordPress admin interface
Version: 1.0
Author: Am2b Dev Team
Author URI: am2bmarketing.co.th
License: GPLv2
*/

add_filter( 'the_generator', 'am2b_change_meta_tag', 10, 2 );
function am2b_change_meta_tag ( $html, $type ) {
    if ( $type == 'xhtml' ) {
        $html = preg_replace( '("WordPress.*?")',
            '"Narit Charoenval"', $html );
    }
    return $html;
}