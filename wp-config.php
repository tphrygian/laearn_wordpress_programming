<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'software_park_01' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'B=LW.<y6}-b^OYXyan&k%+zQK`*>L[p$W[XuT5>gk5}R5,]UvvvApabES_o; TD0' );
define( 'SECURE_AUTH_KEY',  'Pj>x@s=5@H,MVaTsF,QF1@)!@`AxF?G1(07XyI`}Qok17&oIstU>xd%iK)[%XYZM' );
define( 'LOGGED_IN_KEY',    'MW-7$X=H;hUa|#EKbB?I,r!w+O]Zg*WbOJN|t{svtU}^%rZ2Ly)tLpskMkjHUv-~' );
define( 'NONCE_KEY',        'g?B*gh6l:Z(FS=IXY1V#hYmC(-P&gl}fQCy$^P?S{GE*/%1ig%s6*>`dd;Y+_p0/' );
define( 'AUTH_SALT',        'SVS7pBs@hi2_eYxl^xB^6~2/_In3ZcH~0R^>Cq$qY ~}=RP<ha!+vF7n76,6-h1%' );
define( 'SECURE_AUTH_SALT', 'u_RxK`Yu7(1#%#).N={5?|eZN?P%8w!h[NT>Ci>I1GRkDq[aMY}ntA:ANY#e,duI' );
define( 'LOGGED_IN_SALT',   '=aO<1b%Z:,*Q|8<30nb?Pi0r!s;v+GBEZT0JEw:`g`nY*4!}H>oF>ykK*jD{CeGB' );
define( 'NONCE_SALT',       '2S|]13Vam,6DSuD(3DOcdO6<iDI)S$z,]-D<IZa)A^KxHVGbg;N&)Z0}R!8(@F%O' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
